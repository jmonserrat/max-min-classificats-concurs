DIR=/usr
LIBS=\
	-lgecodeflatzinc	-lgecodedriver \
	-lgecodegist		-lgecodesearch \
	-lgecodeminimodel	-lgecodeset \
	-lgecodefloat		-lgecodeint \
	-lgecodekernel		-lgecodesupport

max-classificats : max-classificats.cpp
	g++ -I$(DIR)/include -c max-classificats.cpp
	g++ -L$(DIR)/lib -o max-classificats max-classificats.o $(LIBS)

min-classificats : min-classificats.cpp
	g++ -I$(DIR)/include -c min-classificats.cpp
	g++ -L$(DIR)/lib -o min-classificats min-classificats.o $(LIBS)

all : max-classificats min-classificats

# input : input.cpp
# 	g++ -I$(DIR)/include -c input.cpp
# 	g++ -L$(DIR)/lib -o input input.o $(LIBS)
