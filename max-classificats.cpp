#include <gecode/driver.hh>
#include <gecode/int.hh>
#include <gecode/minimodel.hh>

using namespace Gecode;

typedef std::vector<int> VI;

int n; // # jutges
int m; // # músics
int d; // # vots per jutge
int k; // # vots per classificar-se

class MaxClassificats : public IntMaximizeScript {
protected:
  /// # músics classificats
  IntVar z;
  /// Matriu a[i][j] = 1 si del jutge i ha votat al músic j.
  BoolVarArray a;
  /// Matriu x[i] = 1 si el músic i es classifica
  BoolVarArray x;
public:
  MaxClassificats(const Options& opt)
    : IntMaximizeScript(opt),
      a(*this, n*m, 0, 1),
      x(*this, m, 0, 1),
      z(*this, 0, n*d/k) {

	Matrix<BoolVarArray> A(a, m, n);

	for (int i=0; i<n; i++)
	  rel(*this, sum(A.row(i)) == d);

	for (int i=0; i<m; i++)
	  rel(*this, sum(A.col(i)) + k*(1-x[i]) >= k);

	rel(*this, sum(x) == z); 

    branch(*this, a, INT_VAR_MIN_MIN(), INT_VAL_MAX());
    branch(*this, x, INT_VAR_MIN_MIN(), INT_VAL_MAX());
  }
  MaxClassificats(bool share, MaxClassificats& s)
    : IntMaximizeScript(share, s) {
    a.update(*this, share, s.a);
    x.update(*this, share, s.x);
    z.update(*this, share, s.z);
  }
  virtual Space* copy(bool share) {
    return new MaxClassificats(share,*this);
  }
  virtual IntVar cost(void) const {
    return z;
  }
  virtual void print(std::ostream& os) const {
    os << a << std::endl;
    os << x << std::endl;
    os << z << std::endl;
  }
};

int main(int argc, char* argv[]) {

  std::cin >> n >> m >> d >> k;
  
  // commandline options
  Options opt("Màxim número de músics classificats");
  opt.solutions(0);
  opt.parse(argc,argv);
  // run script
  Script::run<MaxClassificats,BAB,Options>(opt);
  return 0;
}
